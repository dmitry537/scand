﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scand.Data.Entities
{
    public class Subscription 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal PriceInETHPerMonth { get; set; }
        public decimal PriceInBTCPerMonth { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
