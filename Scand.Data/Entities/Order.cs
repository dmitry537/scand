﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scand.Data.Entities
{
    public class Order 
    {
        public int Id { get; set; }
        public string UserId { get; set;  }
        public AppUser User { get; set; }
        public int SubscriptionId { get; set; }
        public Subscription Subscription { get; set; }
        public int Months { get; set; }
        public decimal Amount { get; set; }
        public Currency Currency { get; set; }
        public string AddressToPay { get; set; }
        public string PrivateKey { get; set; }
        public Status Status { get; set; }
        public DateTime Created { get; set; }
    }

    public enum Currency {
        ETH,
        BTC
    }

    public enum Status
    {
        Pending,
        NotPaid,
        Сonfirmation,
        Success
    }
}
