﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Scand.Data.Migrations
{
    public partial class AddOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PriceInSatoshiPerMonth",
                table: "Subscriptions");

            migrationBuilder.DropColumn(
                name: "PriceInWheiPerMonth",
                table: "Subscriptions");

            migrationBuilder.AddColumn<decimal>(
                name: "PriceInBTCPerMonth",
                table: "Subscriptions",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PriceInETHPerMonth",
                table: "Subscriptions",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PriceInBTCPerMonth",
                table: "Subscriptions");

            migrationBuilder.DropColumn(
                name: "PriceInETHPerMonth",
                table: "Subscriptions");

            migrationBuilder.AddColumn<long>(
                name: "PriceInSatoshiPerMonth",
                table: "Subscriptions",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "PriceInWheiPerMonth",
                table: "Subscriptions",
                nullable: true);
        }
    }
}
