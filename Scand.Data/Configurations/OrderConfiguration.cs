﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Scand.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scand.Data.Configurations
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Amount).HasColumnType("decimal(28, 18)");
            builder.HasOne(x => x.Subscription)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.SubscriptionId);
            builder.HasOne(x => x.User)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.UserId);
        }
    }
}
