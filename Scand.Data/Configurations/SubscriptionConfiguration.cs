﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Scand.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scand.Data.Configurations
{
    public class SubscriptionConfiguration : IEntityTypeConfiguration<Subscription>
    {
        public void Configure(EntityTypeBuilder<Subscription> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.PriceInBTCPerMonth).HasColumnType("decimal(28, 18)");
            builder.Property(x => x.PriceInETHPerMonth).HasColumnType("decimal(28, 18)");
            builder.HasMany(x => x.Orders)
                .WithOne(x => x.Subscription)
                .HasForeignKey(x => x.SubscriptionId);
        }
    }
}
