# Scand test app

Для запуска приложения требуется:
  - dotnet core 2.2 runtime
  - postgresql

Что бы запустить приложение необходимо:
1. В файле Scand.Web/appsettings.json  указать строку подлючения к postgresql
2. Установить зависимости npm, в папке Scand.Web запустить npm i
2.2. если при установке зависимостей, вы получите ошибку "Cannot open include file: 'cairo.h'"  - [установите gtk](https://github.com/benjamind/delarre.docpad/blob/master/src/documents/posts/installing-node-canvas-for-windows.html.md "установите gtk")  и повторите установку зависимостей
3. Опубликуйте проект командой  dotnet publish --configuration Release
4. Запустить dotnet Scand.Wb.dll в папке с опубликованным проектом