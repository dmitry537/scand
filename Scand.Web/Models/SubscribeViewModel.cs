﻿using Scand.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scand.Web.Models
{
    public class SubscribeViewModel
    {
        public int SubscriptionId { get; set; }
        public Currency Currency { get; set; }
        public int Months { get; set; }
    }
}
