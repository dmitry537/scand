﻿using Scand.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scand.Web.Models
{
    public class OrderViewModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string SubscriptionName { get; set; }
        public int Months { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string AddressToPay { get; set; }
        public string Status { get; set; }
        public DateTime Created { get; set; }
    }
}
