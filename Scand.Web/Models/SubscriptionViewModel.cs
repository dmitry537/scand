﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scand.Web.Models
{
    public class SubscriptionViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal PriceInETHPerMonth { get; set; }
        public decimal PriceInBTCPerMonth { get; set; }
    }
}
