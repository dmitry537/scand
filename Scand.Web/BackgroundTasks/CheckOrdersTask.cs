﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Scand.Blockchain;
using Scand.Data;
using Scand.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Scand.Web.BackgroundTasks
{
    internal class CheckOrdersTask : IHostedService, IDisposable
    {
        private readonly ILogger _logger;
        private readonly Bitcoin _bitcoin;
        private readonly Ethereum _ethereum;
        private readonly IConfiguration _configuration;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private Timer _timer;

        public CheckOrdersTask(ILogger<CheckOrdersTask> logger, Bitcoin bitcoin, IConfiguration configuration, Ethereum ethereum, IServiceScopeFactory serviceScopeFactory)
        {
            _logger = logger;
            _ethereum = ethereum;
            _bitcoin = bitcoin;
            _configuration = configuration;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is starting.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(20));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            try
            {

                _logger.LogInformation("Timed Background Service is working.");
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var db = scope.ServiceProvider.GetRequiredService<ScandDbContext>();

                    var orders = db.Orders.Where(o => o.Status == Status.Pending || o.Status == Status.Сonfirmation);

                    foreach (var order in orders)
                    {

                        if (order.Currency == Currency.BTC)
                        {
                            var bitcoinBalance = _bitcoin.GetBalance(order.AddressToPay);

                            if (bitcoinBalance.Balance >= order.Amount)
                            {
                                order.Status = Status.Success;
                                var txId = _bitcoin.Transfer(order.PrivateKey, bitcoinBalance.Balance, _configuration["Bitcoin:Address"]).Result;
                                _logger.LogInformation($"Transfer {bitcoinBalance.Balance} transaction id {txId} to app address");
                            }
                            else if (bitcoinBalance.FinalBalance >= order.Amount)
                            {
                                order.Status = Status.Сonfirmation;
                            }
                            else if (DateTime.Now.Subtract(order.Created) > TimeSpan.FromHours(24))
                                order.Status = Status.NotPaid;
                        }
                        else if (order.Currency == Currency.ETH)
                        {
                            var ethereumBalance = _ethereum.GetBalance(order.AddressToPay).Result;

                            if (ethereumBalance >= order.Amount)
                            {
                                order.Status = Status.Success;
                                var txId = _ethereum.Transfer(order.PrivateKey, ethereumBalance, _configuration["Ethereum:Address"]).Result;
                                _logger.LogInformation($"Transfer {ethereumBalance} transaction id {txId} to app address");
                            }
                            else if (DateTime.Now.Subtract(order.Created) > TimeSpan.FromHours(24))
                                order.Status = Status.NotPaid;
                        }
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e.StackTrace);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }

}
