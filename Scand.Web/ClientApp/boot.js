import './css/site.css';
import 'bootstrap';
import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Notifications from 'vue-notification';
import DatatableFactory from 'vuejs-datatable';
import VueModalTor from 'vue-modaltor';

Vue.use(DatatableFactory);
Vue.use(VueAxios, axios);
Vue.use(VueRouter);
Vue.use(Notifications);
Vue.use(VueModalTor);

const routes = [
    { path: '/', component: require('./components/home/home.vue.html') },
    { path: '/login', component: require('./components/login/login.vue.html') },
    { path: '/subscriptions', component: require('./components/subscriptions/subscriptions.vue.html') }, 
    { path: '/orders', component: require('./components/orders/orders.vue.html') },
];

const router = new VueRouter({ mode: 'history', routes: routes });
Vue.router = router;

Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
    fetchData: { url: 'auth/userData', method: 'GET', enabled: true },
    loginData: { url: 'auth/login', fetchUser: true },
    rolesVar: 'roles',
    tokenStore: ['localStorage'],
});

new Vue({
    el: '#app-root',
    router, 
    render: h => h(require('./components/app/app.vue.html'))
});