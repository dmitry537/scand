﻿
export default {
    data: () => ({
        email: '',
        password: '',
        auth2: {}
    }),
    created() {
    },
    mounted() {
    },
    methods: {
        login() {
            this.$auth.login({
                data: { email: this.email, password: this.password },
                redirect: '/',
                rememberMe: true
            }).then(res => {
                console.log(res);
            })
                .catch(err => {
                    if (err.response.status === 401)
                        this.$notify({
                            text: 'Неправильный логин или пароль',
                            type: 'error'
                        });
                    else
                        this.$notify({
                            text: err,
                            type: 'error'
                        });
                });
        }
    }
}