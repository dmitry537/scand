import OrderModal from './orderModal/orderModal.vue.html';

export default {
    data: () => ({
        orders: [],
        curentOrder: {},
        showSubscribeModal: false,
        showAddComponent: false,
        showOrderModal: false,
        columns: [
            { label: 'Order Id', field: 'id', align: 'center', filterable: true },
            { label: 'Subscription', field: 'subscriptionName' },
            { label: 'Amount', representedAs(row) {
                return `${row.amount} ${row.currency}` 
            } },
            { label: 'Status', field: 'status', sortable: false },
            { label: 'Created', field: 'created', sortable: false },
        ],
        filter: '',
        page: 1,
        per_page: 10
    }),
    components: {
        OrderModal
    },
    async created() {
        await this.getOrders();
    },
    methods: {
        hideOrderModal() {
            this.showOrderModal = false;
        },
        openOrderModal(order) {
            this.curentOrder = order;
            this.showOrderModal = true;
        },
        async getOrders() {
            this.orders = (await this.axios.get('/order/getmy')).data;
        }
    }
};
