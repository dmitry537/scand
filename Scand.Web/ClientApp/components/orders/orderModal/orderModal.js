import Qrcode from 'v-qrcode';

export default {
    name: 'OrderModal',
    props: {
        visible: {},
        order: {}
    },
    data: () => ({

    }),
    computed: {
        btcQrValue() {
            if (this.order.currency === 'BTC')
                return `bitcoin:${this.order.addressToPay}`;
            else 
            return '';
        }
    },
    components: {
        Qrcode
    },
    created() {
        this.setQr();
    },
    methods: {
        setQr() {

        },
        hide() {
            this.$emit('hide');
        },
    }
};
