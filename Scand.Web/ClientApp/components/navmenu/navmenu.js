
export default {    
    data: () => ({

    }),
    created() {
    },
    methods: {
        logout() {
            this.$auth.logout({
                makeRequest: false,
                redirect: '/login'
            });  
        }
    }
};
