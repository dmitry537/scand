var Big = require('big.js');

export default {
    name: 'SubscribeModal',
    props: {
        visible: {},
        subscription: {}
    },
    data: () => ({
        months: 1,
        currency: 0,
        currencies: [
            {
                id: 0,
                name: 'ETH'
            },
            {
                id: 1,
                name: 'BTC'
            }
        ]
    }),
    computed: {
        amount() {
            if (this.currency === 0)
                return new Big(this.months).mul(this.subscription.priceInETHPerMonth ? this.subscription.priceInETHPerMonth : 0 );
            else if (this.currency === 1)
                return new Big(this.months).mul(this.subscription.priceInBTCPerMonth ? this.subscription.priceInBTCPerMonth : 0 );
        }
    },
    components: {
    },
    async created() {
    },
    methods: {
        hide() {
            this.$emit('hide');
        },
        async subscribe() {
            var order = (await this.axios.post('/subscription/subscribe', {
                    subscriptionId: this.subscription.id,
                    currency: this.currency,
                    months: parseInt(this.months)
            })).data;
            this.$emit('newOrder', order);
        }
    }
};
