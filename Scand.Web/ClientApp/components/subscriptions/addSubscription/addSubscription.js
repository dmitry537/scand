export default {
    name: 'AddSubscription',
    data: () => ({
        name: '',
        priceInETHPerMonth: 0.0,
        priceInBTCPerMonth: 0.0
    }),
    components: {
        
    },
    created() {

    },
    methods: {
        async addSubscription() {
            var subscription = (await this.axios.post('/subscription/add', {
                name: this.name,
                priceInETHPerMonth: this.priceInETHPerMonth,
                priceInBTCPerMonth: this.priceInBTCPerMonth
            })).data;
            this.$emit('subscriptionAdded', subscription);
        },

    }
};
