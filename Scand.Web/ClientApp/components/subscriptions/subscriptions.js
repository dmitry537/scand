import AddSubscription from './addSubscription/addSubscription.vue.html';
import SubscribeModal from './subscribeModal/subscribeModal.vue.html';
import OrderModal from '../orders/orderModal/orderModal.vue.html';

export default {
    data: () => ({
        subscriptions: [],
        curentSubscription: {},        
        curentOrder: {},
        showSubscribeModal: false,
        showAddComponent: false,
        showOrderModal: false,
        columns: [
            { label: 'Subscription Id', field: 'id', align: 'center', filterable: true },
            { label: 'Name', field: 'name' },
            { label: 'Price in ETH per month', field: 'priceInETHPerMonth', },
            { label: 'Price in BTC per month', field: 'priceInBTCPerMonth', sortable: false },
            { label: '', field: 'empty', sortable: false },
        ],
        filter: '',
        page: 1,
        per_page: 10
    }),
    components: {
        AddSubscription,
        SubscribeModal,
        OrderModal
    },
    async created() {
        await this.getSubscriptions();
    },
    methods: {
        newOrder(order) {
            this.curentOrder = order;
            this.hideSubscribeModal();
            this.openOrderModal();
        },
        hideSubscribeModal() {
            this.showSubscribeModal = false;
        },
        openSubscribeModal(subscription) {
            this.curentSubscription = subscription;
            this.showSubscribeModal = true;
        },
        hideOrderModal() {
            this.showOrderModal = false;
        },
        openOrderModal() {
            this.showOrderModal = true;
        },
        async getSubscriptions() {
            this.subscriptions = (await this.axios.get('/subscription/getall')).data;
        },
        async remove(subscription) {
            await this.axios.delete('/subscription/delete', {
                params: {
                    subscriptionId: subscription.id
                }
            });
            this.subscriptions = this.subscriptions.filter(s => s.id !== subscription.id);
        },
        subscriptionAdded(subscription) {
            this.subscriptions.push(subscription);
            this.showAddComponent = false;
        },
        showAddForm() {
            this.showAddComponent = true;
        },
        hideAddForm() {
            this.showAddComponent = false;
        }
    }
};
