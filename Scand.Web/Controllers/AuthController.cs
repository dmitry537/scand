﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Scand.Data.Entities;
using Scand.Web.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Scand.Web.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AuthController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly UserManager<AppUser> _userManager;
        public AuthController(IConfiguration configuration, UserManager<AppUser> userManager)
        {
            _configuration = configuration;
            _userManager = userManager;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]AuthViewModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
                return BadRequest("Invalid email");            

            if (!await _userManager.CheckPasswordAsync(user, model.Password))
                return BadRequest("Invalid email or password");

            var now = DateTime.UtcNow;

            var claims = await _userManager.GetClaimsAsync(user);

            var jwt = new JwtSecurityToken(
                    issuer: _configuration["Tokens:Issuer"],
                    notBefore: now,
                    claims: claims,                    
                    expires: now.Add(TimeSpan.FromSeconds(int.Parse(_configuration["Tokens:Lifetime"]))),
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Tokens:Key"])), SecurityAlgorithms.HmacSha256)
                    );

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            Response.Headers.Add("Authorization", "Bearer " + encodedJwt);

            return Ok();
        }

        public async Task<IActionResult> UserData()
        {
            var user = await _userManager.GetUserAsync(User);
            var roles = await _userManager.GetRolesAsync(user);
            return Json(new { data = new { roles } });
        }
    }
}
