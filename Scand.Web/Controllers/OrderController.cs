﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Scand.Blockchain;
using Scand.Data;
using Scand.Data.Entities;
using Scand.Web.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Scand.Web.Controllers
{
    public class OrderController : Controller
    {
        private readonly ScandDbContext _db;
        private readonly UserManager<AppUser> _userManager;
        public OrderController(ScandDbContext db, UserManager<AppUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet]
        public async Task<IActionResult> GetMy()
        {
            var user = await _userManager.GetUserAsync(User);
            var res = _db.Orders
                .Include(o => o.User)
                .Include(o => o.Subscription)
                .Where(o => o.UserId == user.Id)
                .Select(o => new OrderViewModel()
                {
                    Id = o.Id,
                    UserName = o.User.UserName,
                    SubscriptionName = o.Subscription.Name,
                    Months = o.Months,
                    Amount = o.Amount,
                    Currency = o.Currency.ToString(),
                    AddressToPay = o.AddressToPay,
                    Status = o.Status.ToString(),
                    Created = o.Created
                }).ToList();

            return Json(res);
        }        
    }
}
