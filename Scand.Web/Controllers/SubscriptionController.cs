﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Scand.Blockchain;
using Scand.Data;
using Scand.Data.Entities;
using Scand.Web.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Scand.Web.Controllers
{
    public class SubscriptionController : Controller
    {
        private readonly ScandDbContext _db;
        private readonly UserManager<AppUser> _userManager;
        private readonly Bitcoin _bitcoin;
        private readonly Ethereum _ethereum;
        public SubscriptionController(ScandDbContext db, UserManager<AppUser> userManager, Bitcoin bitcoin, Ethereum ethereum)
        {
            _db = db;
            _userManager = userManager;
            _bitcoin = bitcoin;
            _ethereum = ethereum;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody]SubscriptionViewModel model)
        {
            var subscription = new Subscription()
            {
                Name = model.Name,
                PriceInBTCPerMonth = model.PriceInBTCPerMonth,
                PriceInETHPerMonth = model.PriceInETHPerMonth
            };

            _db.Subscriptions.Add(subscription);

            await _db.SaveChangesAsync();

            return Json(subscription);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost]
        public async Task<IActionResult> Subscribe([FromBody]SubscribeViewModel model)
        {
            var user = await _userManager.GetUserAsync(User);
            var subscription = _db.Subscriptions.FirstOrDefault(s => s.Id == model.SubscriptionId);

            if (subscription == null)
                return BadRequest("Subscription does not exist");

            var address = "";
            var privateKey = "";
            var amount = 0.0M;

            if (model.Currency == Currency.BTC)
            {
                amount = model.Months * subscription.PriceInBTCPerMonth;
                _bitcoin.GenerateAddress(out address, out privateKey);
            }                  
            else if (model.Currency == Currency.ETH)
            {
                amount = model.Months * subscription.PriceInETHPerMonth;
                _ethereum.GenerateAddress(out address, out privateKey);
            }              
                       
            var order = new Order() {
                AddressToPay = address,
                PrivateKey = privateKey,
                Amount = amount,
                Currency = model.Currency,
                SubscriptionId = subscription.Id,
                Months = model.Months,
                Status = Status.Pending,
                UserId = user.Id   ,
                Created = DateTime.Now
            };

            _db.Orders.Add(order);

            await _db.SaveChangesAsync();                      

            return Json(new OrderViewModel() {
                AddressToPay = order.AddressToPay,
                Amount = order.Amount,
                Currency = order.Currency.ToString(),
                Created = order.Created,
                Months = order.Months,
                Status = order.Status.ToString(),
                SubscriptionName = subscription.Name,
                UserName = user.UserName
            });
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet]
        public IActionResult GetAll()
        {
            var res = _db.Subscriptions.Select(s => new SubscriptionViewModel()
            {
                Id = s.Id,
                Name = s.Name,
                PriceInBTCPerMonth = s.PriceInBTCPerMonth,
                PriceInETHPerMonth = s.PriceInETHPerMonth
            }).ToList();

            return Json(res);
        }

        [HttpDelete]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> Delete(int subscriptionId)
        {
            var subscription = _db.Subscriptions.FirstOrDefault(s => s.Id == subscriptionId);
            if (subscription == null)
                return BadRequest("subscription does not exist");

            _db.Subscriptions.Remove(subscription);

            await _db.SaveChangesAsync();

            return Ok();
        }
    }
}
