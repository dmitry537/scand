﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Scand.Data;
using Scand.Data.Entities;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Scand.Web
{
    public interface IDbInitializer
    {
        Task InitializeAsync();
    }

    public class DbInitializer : IDbInitializer
    {
        private readonly ScandDbContext _db;
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public DbInitializer(ScandDbContext db, UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _db = db;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task InitializeAsync()
        {
            await _db.Database.MigrateAsync();
            await SeedUsersAndRolesAsync();
        }

        private async Task SeedUsersAndRolesAsync()
        {
            var adminRoleName = "Admin";
            var userRoleName = "User";

            if (await _roleManager.FindByNameAsync(adminRoleName) == null)
            {
                await _roleManager.CreateAsync(new IdentityRole(adminRoleName));
            }
            if (await _roleManager.FindByNameAsync(userRoleName) == null)
            {
                await _roleManager.CreateAsync(new IdentityRole(userRoleName));
            }

            var adminUser = new AppUser() {
                UserName = "admin@admin.com",
                Email = "admin@admin.com"
            };
            
            if (await _userManager.FindByEmailAsync(adminUser.Email) == null)
            {
                await _userManager.CreateAsync(adminUser, "12345O_o");
                await _userManager.AddToRoleAsync(adminUser, adminRoleName);
                await _userManager.AddClaimsAsync(adminUser, new List<Claim>() {
                    new Claim(JwtRegisteredClaimNames.Sub, adminUser.Id),
                    new Claim(ClaimTypes.NameIdentifier, adminUser.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Email, adminUser.Email),
                    new Claim(ClaimTypes.Role, adminRoleName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                });
            }

            var user = new AppUser()
            {
                UserName = "user@user.com",
                Email = "user@user.com"
            };

            if (await _userManager.FindByEmailAsync(user.Email) == null)
            {
                await _userManager.CreateAsync(user, "12345O_o");
                await _userManager.AddToRoleAsync(user, userRoleName);
                await _userManager.AddClaimsAsync(user, new List<Claim>() {
                    new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Email, user.Email),
                    new Claim(ClaimTypes.Role, userRoleName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                });
            }
        }
    }
}
