﻿using Microsoft.Extensions.Configuration;
using NBitcoin;
using Nethereum.Hex.HexConvertors.Extensions;
using Nethereum.Hex.HexTypes;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Signer;
using Nethereum.Util;
using Nethereum.Web3;
using Nethereum.Web3.Accounts;
using Nethereum.Web3.Accounts.Managed;
using QBitNinja.Client;
using QBitNinja.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scand.Blockchain
{
    public class Ethereum
    {
        private readonly IConfiguration _configuration;
        private readonly Chain _chain;

        public Ethereum(IConfiguration configuration)
        {
            _configuration = configuration;
            if (_configuration["Ethereum:IsTest"] == "True")
                _chain = Chain.Rinkeby;
            else
                _chain = Chain.MainNet;
        }

        public void GenerateAddress(out string address, out string privateKey)
        {
            var ecKey = Nethereum.Signer.EthECKey.GenerateKey();
            privateKey = ecKey.GetPrivateKeyAsBytes().ToHex();
            address = new Account(privateKey, _chain).Address;
        }

        public async Task<decimal> GetBalance(string address)
        {
            var web3 = new Web3(_configuration["Ethereum:InfuraEndpoint"]);
            var bigBalance = await web3.Eth.GetBalance.SendRequestAsync(address);
            return Web3.Convert.FromWei(bigBalance);
        }

        public async Task<string> Transfer(string privateKey, decimal transferValue, string toAddress)
        {
            var account = new Account(privateKey, _chain);
            var web3 = new Web3(account, _configuration["Ethereum:InfuraEndpoint"]);
            var gas = await web3.Eth.GasPrice.SendRequestAsync();
            var amount = Web3.Convert.ToWei(transferValue) - 21000 * gas.Value;
            var txHash = await web3.TransactionManager.SendTransactionAsync(account.Address, toAddress, new HexBigInteger(amount));
            return txHash;
        }
    }
}
