﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scand.Blockchain.Models
{
    public class BlockcypherTxSkeletonModel
    {
        public Tx tx { get; set; }
        public List<string> tosign { get; set; }
        public List<string> signatures { get; set; }
        public List<string> pubkeys { get; set; }
    }

    public class Tx
    {
        public string hash { get; set; }
        public List<Input> inputs { get; set; }
        public List<Output> outputs { get; set; }
        public long total { get; set; }
        public long fees { get; set; }
        public string preference { get; set; }
        public string relayed_by { get; set; }
        public string received { get; set; }
        public long ver { get; set; }
        public bool double_spend { get; set; }
        public long vin_sz { get; set; }
        public long vout_sz { get; set; }
        public long confirmations { get; set; }
    }

    public class Input
    {
        public string prev_hash { get; set; }
        public long output_index { get; set; }
        public long output_value { get; set; }
        public long sequence { get; set; }
        public List<string> addresses { get; set; }
        public string script_type { get; set; }
        public long age { get; set; }
    }

    public class Output
    {
        public long value { get; set; }
        public string script { get; set; }
        public List<string> addresses { get; set; }
        public string script_type { get; set; }
    }
}
