﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scand.Blockchain.Models
{
    public class BlockcypherFeeModel
    {
        public decimal high_fee_per_kb { get; set; }
        public decimal medium_fee_per_kb { get; set; }
        public decimal low_fee_per_kb { get; set; }

    }
}
