﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scand.Blockchain.Models
{
    public class BlockcypherAddrModel
    {
        public decimal balance { get; set; }
        public decimal unconfirmed_balance { get; set; }
        public decimal final_balance { get; set; }

    }
}
