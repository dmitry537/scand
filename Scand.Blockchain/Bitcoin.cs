﻿using Microsoft.Extensions.Configuration;
using NBitcoin;
using NBitcoin.Protocol;
using Newtonsoft.Json;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Sec;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Signers;
using Org.BouncyCastle.Math.EC;
using Scand.Blockchain.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Scand.Blockchain.Extensions;
using Scand.Blockchain.Helpers;
using NBitcoin.BouncyCastle.Math;

namespace Scand.Blockchain
{
    public class Bitcoin
    {
        private readonly IConfiguration _configuration;
        private readonly Network _network;

        public Bitcoin(IConfiguration configuration)
        {
            _configuration = configuration;
            var connectionParameters = new NodeConnectionParameters();
            if (_configuration["Bitcoin:IsTest"] == "True")
            {
                _network = Network.TestNet;
            }
            else
            {
                _network = Network.Main;
            }
        }

        public void GenerateAddress(out string address, out string privateKey)
        {
            var mnemonic = new Mnemonic(Wordlist.English, WordCount.Twelve);

            ExtKey masterKey = mnemonic.DeriveExtKey();
            KeyPath keypth = new KeyPath("m/44'/0'/0'/0/");
            ExtKey key = masterKey.Derive(keypth);
            address = key.PrivateKey.PubKey.GetAddress(_network).ToString();
            privateKey = key.PrivateKey.GetBitcoinSecret(_network).ToString();
        }

        public BitcoinBalance GetBalance(string address)
        {
            var url = _configuration["Bitcoin:BlockcypherAPI"] + "/addrs/" + address;
            var request = WebRequest.Create(url) as HttpWebRequest;
            request.ContentType = "application/json";
            request.Method = "GET";
            var response = request.GetResponse() as HttpWebResponse;
            var webStream = response.GetResponseStream();
            var json = new DataContractJsonSerializer(typeof(BlockcypherAddrModel));
            var bitcoinBalance = (BlockcypherAddrModel)json.ReadObject(webStream);

            return new BitcoinBalance()
            {
                Balance = bitcoinBalance.balance / 100000000,
                FinalBalance = bitcoinBalance.final_balance / 100000000,
                UnconfirmedBalance = bitcoinBalance.unconfirmed_balance / 100000000
            };
        }

        public async Task<string> Transfer(string privateKey, decimal transferValue, string toAddress)
        {
            var secret = new BitcoinSecret(privateKey, _network);

            var fromAddress = secret.GetAddress().ToString();

            var txSkeleton = await NewTransaction(fromAddress, toAddress, transferValue);

            txSkeleton.signatures = new List<string>();
            txSkeleton.pubkeys = new List<string>();

            Sign(txSkeleton, secret, true, true);

            await SendTransaction(txSkeleton);

            return txSkeleton.tx.hash;
        }


        private async Task<BlockcypherTxSkeletonModel> SendTransaction(BlockcypherTxSkeletonModel txSkeleton)
        {
            var url = _configuration["Bitcoin:BlockcypherAPI"] + "/txs/send";

            var client = new HttpClient();

            var response = await client.PostAsync(url, new StringContent(JsonConvert.SerializeObject(txSkeleton), Encoding.UTF8, "application/json"));

            if (!response.IsSuccessStatusCode)
                throw new Exception($"Invalid responce from blockcypher: {response.ReasonPhrase}");

            var webStream = await response.Content.ReadAsStreamAsync();
            var json = new DataContractJsonSerializer(typeof(BlockcypherTxSkeletonModel));
            txSkeleton = (BlockcypherTxSkeletonModel)json.ReadObject(webStream);

            return txSkeleton;
        }

        private async Task<BlockcypherTxSkeletonModel> NewTransaction(string fromAddress, string toAddress, decimal value)
        {
            var feePerKb = GetFeePerKb();
            var url = _configuration["Bitcoin:BlockcypherAPI"] + "/txs/new";

            var client = new HttpClient();
            var fees = (long)Math.Round(feePerKb / 4);
            var amount = new Money(value, MoneyUnit.BTC);
            var body = new
            {
                inputs = new List<object>() {
                    new {
                         addresses = new List<string>() {
                                fromAddress.ToString()
                            }
                    }
                },
                outputs = new List<object>()
                {
                    new {
                         addresses = new List<string>() {
                                _configuration["Bitcoin:Address"]
                            },
                          value = amount.Satoshi - fees
                    }
                },
                fees = fees
            };

            var response = await client.PostAsync(url, new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json"));

            if (!response.IsSuccessStatusCode)
                throw new Exception($"Invalid responce from blockcypher: {response.ReasonPhrase}");

            var webStream = await response.Content.ReadAsStreamAsync();
            var json = new DataContractJsonSerializer(typeof(BlockcypherTxSkeletonModel));
            var txSkeleton = (BlockcypherTxSkeletonModel)json.ReadObject(webStream);

            return txSkeleton;
        }

        private decimal GetFeePerKb()
        {
            var url = _configuration["Bitcoin:BlockcypherAPI"];
            var request = WebRequest.Create(url) as HttpWebRequest;
            request.ContentType = "application/json";
            request.Method = "GET";

            var response = request.GetResponse() as HttpWebResponse;
            var webStream = response.GetResponseStream();
            var json = new DataContractJsonSerializer(typeof(BlockcypherFeeModel));
            var chainFee = (BlockcypherFeeModel)json.ReadObject(webStream);
            return chainFee.medium_fee_per_kb;
        }

        private static void Sign(BlockcypherTxSkeletonModel unsignedTransaction, BitcoinSecret secret, bool isHex, bool addPubKey,
                                 bool forceCompressed = false)
        {
            var compressed = secret.PrivateKey.IsCompressed;
            var bytes = secret.PrivateKey.ToBytes();

            if (bytes.Length == 33 && bytes[32] == 1)
            {
                compressed = true;
                bytes = bytes.Take(32).ToArray();
            }

            var privKeyB = new Org.BouncyCastle.Math.BigInteger(1, bytes);
            var parms = SecNamedCurves.GetByName("secp256k1");
            var curve = new ECDomainParameters(parms.Curve, parms.G, parms.N, parms.H);
            var halfCurveOrder = parms.N.ShiftRight(1);

            var point = curve.G.Multiply(privKeyB);

            if (compressed || forceCompressed)
                point = new FpPoint(curve.Curve, point.X, point.Y, true);

            var publicKey = point.GetEncoded();
            var signer = new ECDsaSigner();
            var privKey = new ECPrivateKeyParameters(privKeyB, curve);
            signer.Init(true, privKey);

            foreach (string toSign in unsignedTransaction.tosign)
            {
                if (addPubKey)
                    unsignedTransaction.pubkeys.Add(publicKey.ToHexString());

                var components = signer.GenerateSignature(toSign.FromHexString());
                var r = components[0];
                var s = components[1];

                if (s.CompareTo(halfCurveOrder) > 0)
                    s = curve.N.Subtract(s);

                using (var ms = new MemoryStream())
                using (var asn = new Asn1OutputStream(ms))
                {
                    var seq = new DerSequenceGenerator(asn);
                    seq.AddObject(new DerInteger(r));
                    seq.AddObject(new DerInteger(s));

                    seq.Close();

                    string signedString = ms.ToArray().ToHexString();

                    unsignedTransaction.signatures.Add(signedString);
                }
            }
        }

        private static byte[] GetBytesFromBase58Key(string privateKey)
        {
            var tmp = Base58Helper.DecodeWithCheckSum(privateKey);
            var bytes = new byte[tmp.Length - 1];

            Array.Copy(tmp, 1, bytes, 0, tmp.Length - 1);

            return bytes;
        }

    }
}
