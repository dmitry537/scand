﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scand.Blockchain
{
    public class BitcoinBalance
    {
        public decimal Balance { get; set; }
        public decimal UnconfirmedBalance { get; set; }
        public decimal FinalBalance { get; set; }
    }
}
